package otros;

import java.util.Iterator;
import java.util.List;

public class Destino {
	private String lugar;
	private double distancia; //en km
	
	
	public Destino() { //utilizado para blanquear un viaje en clase transporte
		this.lugar = "";
		this.distancia = -1;
	}
	public Destino(String destino, int distancia) {
		this.lugar = destino;
		if (distancia < 0)			
			throw new RuntimeException("distancia no puede ser inferior a cero"); 
		else
			this.distancia = distancia;
	}

	
/////////////////Getters y Setters//////////////////
	//string destino
	public void lugar(String s) {this.lugar = s;}
	public String lugar() {return this.lugar;}
	//int distancia
	public void distancia(double d) {this.distancia = d;}
	public double distancia() {return this.distancia;}
	
/////////////////Metodos de clase//////////////////	
	public static Destino find(List<Destino> list, Destino destino) {
		Iterator<Destino> it = list.iterator();
		while (it.hasNext()) {
			Destino d = it.next();
			if (d.lugar() == destino.lugar())
				return d;
		}
		return null;
	}
	
	public static Destino find(List<Destino> list, String Destinolugar) {
		Iterator<Destino> it = list.iterator();
		while (it.hasNext()) {
			Destino d = it.next();
			if (d.lugar() == Destinolugar)
				return d;
		}
		return null;
	}

}
