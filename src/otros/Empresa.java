package otros;

import mercaderia.Deposito;
import mercaderia.Paquete;
import flota.Flete;
import flota.MegaTrailer;
import flota.TrailerComun;
import flota.Transporte;

import java.util.LinkedList;
import java.util.List;

public class Empresa {
	private String nombre;
	private String cuit;
	private List<Deposito> depositos;
	private List<Transporte> transportes;
	private List<Destino> destinos;
	
	public Empresa(String cuit, String nombre) {
		this.nombre = nombre;
		String numeros = "0123456789";
		for (int i = 0 ; i < cuit.length() ; i++) {
			if (numeros.indexOf(cuit.charAt(i)) == -1) {
				throw new RuntimeException("El CUIT ingresado no es v�lido"); 
			}
		}
		this.cuit = cuit;
		this.depositos = new LinkedList<Deposito>();
		this.transportes = new LinkedList<Transporte>();
		this.destinos = new LinkedList<Destino>();
	}
	
/////////////////Getters y Setters//////////////////
	//String nombre
	public void nombre(String s) {this.nombre = s;}
	public String nombre() {return this.nombre;}
	//int cuit
	public void cuit(String s) {this.cuit = s;}
	public String cuit() {return this.cuit;}
	
/////////////////Metodos de clase//////////////////	
	@Override
	public String toString() {		
		return this.nombre + " - " + this.cuit;		
	}

	public String obtenerTransporteIgual(String idTransp) {
		Transporte t = Transporte.find(this.transportes, idTransp); //busca un transporte por id
		if (t == null) 
			throw new RuntimeException("El transporte " + idTransp + "no existe"); 
		Transporte t2 = Transporte.findTransporteIgual(this.transportes, t); //busca un transporte que comparta caracteristicas con t
		if (t2 == null) 
			throw new RuntimeException("No existe un transporte igual a " + idTransp); 
		return t2.id();
	}

	public int agregarDepTercerizFrio(double capacidad, double costo) {
		int id = this.depositos == null ? 0 : this.depositos.size(); //el id de un deposito es su indice en el array
		Deposito d = new Deposito(id, capacidad, true, false, costo);
		this.depositos.add(d);
		return d.id();
	}

	public int agregarDeposito(double volumen, boolean frigorifico, boolean propio) {
		int id = this.depositos == null ? 0 : this.depositos.size(); //el id de un deposito es su indice en el array
		Deposito d = new Deposito(id, volumen, frigorifico, propio, 0);
		this.depositos.add(d);
		return id;
	}

	public void agregarDestino(String destino, int km) {
		Destino d = new Destino(destino, km);
		if (Destino.find(this.destinos, d) == null) {
			this.destinos.add(d);
		}
	}

	public void agregarTrailer(String idTransp, double cargaMax, double capacidad,
							   boolean frigorifico, double costoKm, double segCarga) {
		Transporte t = new TrailerComun(idTransp, cargaMax, capacidad, frigorifico, costoKm, segCarga);
		this.transportes.add(t);
	}

	public void agregarMegaTrailer(String idTransp, double cargaMax, double capacidad, boolean frigorifico, 
								   double costoKm, double segCarga, double costoFijo, double comida) {
		Transporte t = new MegaTrailer(idTransp, cargaMax, capacidad, frigorifico, costoKm, segCarga, comida, costoFijo);
		this.transportes.add(t);		
	}

	public void agregarFlete(String idTransp, double cargaMax, double capacidad, 
							 double costoKm, int acomp, double costoPorAcom) {
		Transporte t = new Flete(idTransp, cargaMax, capacidad, costoKm, acomp, costoPorAcom);
		this.transportes.add(t);	
	}

	public void asignarDestino(String idTransp, String destino) {
		Transporte t = Transporte.find(this.transportes, idTransp);
		if (t == null) 
			throw new RuntimeException("El transporte " + idTransp + "no existe");
		Destino d = Destino.find(this.destinos, destino);
		if (d == null) 
			throw new RuntimeException("El destino " + destino + "no existe");
		t.destino(d);
	}

	public boolean incorporarPaquete(String destino, double peso, double volumen, boolean frio) {
		Paquete p = new Paquete(destino, volumen, peso, frio);
		return Deposito.addPaquete(this.depositos, p); //incorpora un paquete a un deposito compatible
	}

	public double cargarTransporte(String idTransp) {
		Transporte t = Transporte.find(this.transportes, idTransp); //busca un transporte por id
		if (t == null)
			throw new RuntimeException("No se pudo cargar la mercaderia. No se encontro el transporte con id: " + idTransp); 
		if (t.enViaje() || !t.poseeDestinoAsignado())
			throw new RuntimeException("El transporte " + idTransp + "no tiene asignado un destino o se encuentra en viaje"); 
		return t.cargarTransporte(this.depositos);  //carga un transporte a maxima capacidad buscando paquetes cargables en todos los depositos
	}

	public void iniciarViaje(String idTransp) {
		Transporte t = Transporte.find(this.transportes, idTransp); //busca un transporte por id
		if (t == null) 
			throw new RuntimeException("No se pudo iniciar el viaje. No se encontro el transporte con id: " + idTransp); 
		if (t.enViaje() || !t.poseeDestinoAsignado() || !t.poseeMercaderiaCargada()) 
			throw new RuntimeException("El transporte con id: " + idTransp + " esta en viaje o no tiene destino asignado o no posee mercaderia cargada");
		t.enViaje(true);
		
	}

	public double obtenerCostoViaje(String idTransp) {
		Transporte t = Transporte.find(this.transportes, idTransp); //busca un transporte por id
		if(t == null) throw new RuntimeException("No se encontro el transporte con id: " + idTransp);
		return t.costoViaje() + t.costoPaquetes(); //calcula el costo de viaje segun el tipo de transporte y suma el costo de los paquetes cargados	
	}

	public void finalizarViaje(String idTransp) {
		Transporte t = Transporte.find(this.transportes, idTransp); //busca un transporte por id
		if(t == null) throw new RuntimeException("No se encontro el transporte con id: " + idTransp);
		t.vaciarTransporte(); //vacia un transporte (elimina los paquetes, ya no estan en esta empresa ni en el camion)
		t.deleteDestino(); 
		t.enViaje(false); 
	}
	
}
