package flota;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import mercaderia.Deposito;
import mercaderia.Paquete;
import otros.Destino;

public abstract class Transporte {
	protected String id; //identificador unico
	protected double KgMax;  
	protected double VolMax;  
	protected boolean refrigeracion; 
	protected double costoPorKm; //en $
	
	protected double KgActual; 
	protected double VolActual; 
	protected double costoPaquetes; //en $
	protected boolean enViaje;
	
	protected Destino destino;
	protected List<Paquete> paquetes;
	
	public Transporte(String id, double maxKg, double maxL, boolean refrigeracion, double costoPorKm) {
		this.KgActual = 0;
		this.VolActual = 0;
		this.costoPaquetes = 0;
		this.enViaje = false;
		this.destino = new Destino();
		this.paquetes = new LinkedList<Paquete>();
		if (id == null || id == "")
			throw new RuntimeException("El ID de un transporte no puede ser nulo o vacio"); 
		this.id = id;
		this.refrigeracion = refrigeracion;
		if (maxKg < 0)
			throw new RuntimeException("Kilos maximos de carga no puede ser inferior a cero"); 
		this.KgMax = maxKg;
		if (maxL < 0)			
			throw new RuntimeException("Litros maximos de carga no puede ser inferior a cero"); 
		this.VolMax = maxL;
		if (costoPorKm < 0)			
			throw new RuntimeException("el costo por Km recorrido no puede ser inferior a cero"); 
		this.costoPorKm = costoPorKm;
	}
	
	
/////////////////Getters y Setters//////////////////
	//int id
	public String id() {return this.id;}
	//int maxKg
	public void KgMax(double d) {this.KgMax = d;}
	public double KgMax() {return this.KgMax;}
	//int maxL
	public void VolMax(double d) {this.VolMax = d;}
	public double VolMax() {return this.VolMax;}
	//double costoPorKm
	public void costoPorKm(double d) {this.costoPorKm = d;}
	public double costoPorKm() {return this.costoPorKm;}
	//int cargaKg
	public void KgActual(double d) {this.KgActual = d;}
	public double KgActual() {return this.KgActual;}
	//int cargaL
	public void VoActual(double d) {this.VolActual = d;}
	public double VolActual() {return this.VolActual;}
	//double costoTotal
	public void costoPaquetes(double d) {this.costoPaquetes = d;}
	public double costoPaquetes() {return this.costoPaquetes;}
	//boolean enViaje
	public void enViaje(boolean b) {this.enViaje = b;}
	public boolean enViaje() {return this.enViaje;}
	//Viaje viaje
	public void destino(Destino v) {this.destino = v;}
	public Destino destino() { return this.destino;}
	//Paquete paquete
	public void paquete(List<Paquete> p) {this.paquetes = p;}
	public List<Paquete> paquete() { return this.paquetes;}
	//tipo de transporte
	public String tipo() {return "transporte";}

/////////////////Metodos de clase//////////////////
	
	public static Transporte find(List<Transporte> list, String idTransp) {
		Iterator<Transporte> it = list.iterator();
		while (it.hasNext()) {
			Transporte t = it.next();
			if (t.id() == idTransp)
				return t;
		}
		return null;
	}

	public static Transporte findTransporteIgual(List<Transporte> list, Transporte t) {
		Iterator<Transporte> it = list.iterator();
		while (it.hasNext()) {
			Transporte t2 = it.next();
			if (Transporte.equals(t,t2))
					return t2;
		}
		return null;
	}
	
	public static boolean equals(Transporte t, Transporte t2) {
		return (t2.KgActual() == t.KgActual() &&
				t2.VolActual() == t.VolActual() &&
				t2.destino().lugar() == t.destino().lugar() &&
				t2.tipo() == t.tipo() &&
				t2.id != t.id);
	}
	
	public boolean equals(Transporte t) {
		return (Transporte.equals(this, t));
	}
	
	public boolean poseeDestinoAsignado() {
		return (this.destino.lugar() != "");
	}
	public boolean poseeMercaderiaCargada() {
		return (this.KgActual != 0 && this.VolActual != 0);
	}
	
	public double cargarTransporte(List<Deposito> depositos) {
		double volumenPaquetesCargados = 0;
		double kilosPaquetesCargados = 0;
		double kgCargadosDeUnDeposito = 0;
		double costoDepositosNoPropios = 0;
		List<Paquete> paquetesCargados =new LinkedList<Paquete>();
		for (Deposito deposito : depositos) { //busco en todos los depositos los paquetes que cumplen con la condicion
			if(this.refrigeracion == deposito.frigorifico()) { //'condicion'
				Iterator<Paquete> it = deposito.paquetes().iterator();
				while(it.hasNext()) {
					Paquete p = it.next();
					if (p.destino().lugar() == this.destino().lugar() && volumenPaquetesCargados < (this.VolMax - this.VolActual) && kilosPaquetesCargados < (this.KgMax - this.KgActual)) { //filtro los paquetes que cumplen con la condicion por destino y valido por solo los que entran en el camion (para no tener que devolver paquetes cargables si no pudieron entrar al camion mas adelante) con esta validacion me evito tener que ordenar los paquetes mas adelante para meterlos de manera eficiente (de mas voluminoso a menos voluminoso)
						this.paquetes.add(p);
						kgCargadosDeUnDeposito = kgCargadosDeUnDeposito + p.peso();
						volumenPaquetesCargados = volumenPaquetesCargados + p.volumen();
						kilosPaquetesCargados = kilosPaquetesCargados + p.peso();
						paquetesCargados.add(p); //el paquete ya no estara mas en el deposito
					}
				}
				for(Paquete p:paquetesCargados) {
					deposito.quitarPaquete(p);
				}
				if (!deposito.propio() && kgCargadosDeUnDeposito>1000) {
					costoDepositosNoPropios = kgCargadosDeUnDeposito/1000 * deposito.costo();					
				}
			}
			kgCargadosDeUnDeposito = 0;
		}
		this.KgActual = this.KgActual + kilosPaquetesCargados;
		this.VolActual = this.VolActual + volumenPaquetesCargados;
		this.costoPaquetes = costoDepositosNoPropios;
		return volumenPaquetesCargados;
	}

	public void vaciarTransporte() {
		this.paquetes = new LinkedList<Paquete>();
		this.KgActual(0);
		this.VoActual(0);
		this.costoPaquetes = 0;
	}
	
	public void deleteDestino() {
		this.destino = new Destino(); //uso los valores by default del constructor
	}
	
	public abstract double costoViaje();
	
}
