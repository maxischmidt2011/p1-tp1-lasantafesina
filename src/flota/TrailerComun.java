package flota;

import otros.Destino;

public class TrailerComun extends Transporte {
	final boolean largaDistancia = false; //realiza viajes de hasta 500km
	final double seguroDeCarga;

	
	public TrailerComun(String id, double maxKg, double maxL, boolean refrigeracion, double costoPorKm, double seguroDeCarga) {				
		super(id, maxKg, maxL, refrigeracion, costoPorKm);
		if (seguroDeCarga < 0)			
			throw new RuntimeException("Seguro de carga no puede ser inferior a cero"); 
		this.seguroDeCarga = seguroDeCarga;
	}
	
	public double costoViaje() {
		double costo = this.costoPorKm*this.destino.distancia() + this.seguroDeCarga;
		return costo;
		}
	
/////////////////Metodos de clase//////////////////
	@Override
	public void destino(Destino v) {
		if (v.distancia() > 500)
			throw new RuntimeException("El destino no puede tener una distancia mayor a 500km");
		this.destino = v;
	}
	@Override
	public String tipo() {
		return "TrailerComun";
	}
}

