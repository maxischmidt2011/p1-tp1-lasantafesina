package flota;

public class Flete extends Transporte {
	private int cantPasajeros; //sin contar al conductor
	private double costoPorPasajero; //en $
	
	public Flete(String id, double maxKg, double maxL, double costoPorKm, int cantAcompaņantes, double costoPorAcomp) {		
		super(id, maxKg, maxL, false, costoPorKm);
			if (cantAcompaņantes < 0)			
				throw new RuntimeException("Cantidad de acompaņantes no puede ser inferior a cero"); 
			this.cantPasajeros = cantAcompaņantes;
			if (costoPorAcomp < 0)			
				throw new RuntimeException("Costo fijo no puede ser inferior a cero"); 
			this.costoPorPasajero = costoPorAcomp;
	}

/////////////////Getters y Setters//////////////////
	//int cantPasajeros
	public int getCantPasajeros() {return cantPasajeros;}
	public void setCantPasajeros(Integer cantPasajeros) {this.cantPasajeros = cantPasajeros;}
	//double costoPorPasajero
	public double getCosto() {return costoPorPasajero;}
	public void setCosto(Integer costo) {	this.costoPorPasajero = costo;}
	
/////////////////Metodos de clase//////////////////
	@Override
	public double costoViaje() {
		double costo = (this.costoPorKm*this.destino.distancia()) + (this.costoPorPasajero*this.cantPasajeros);
		return costo;
	}
	@Override
	public String tipo() {
		return "Flete";
	}
	

	
}
