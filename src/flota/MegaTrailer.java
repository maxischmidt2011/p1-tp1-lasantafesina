package flota;

import otros.Destino;

public class MegaTrailer extends Transporte {
	final boolean largaDistancia = true; //realiza viajes de mas de 500km
	private double gastoComida; //en $
	private double costoFijo;
	private double seguroCarga;
	
	
	public MegaTrailer(String id, double maxKg, double maxL, boolean refrigeracion, double costoPorKm, double seguroCarga, double gastoComida, double costoFjo) {		
		super(id, maxKg, maxL, refrigeracion, costoPorKm);
			if (gastoComida < 0)			
				throw new RuntimeException("El gasto por comida no puede ser inferior a cero"); 
			this.gastoComida = gastoComida;
			if (costoFjo < 0)			
				throw new RuntimeException("Costo fijo no puede ser inferior a cero"); 
			this.costoFijo = costoFjo;
			if (seguroCarga < 0)			
				throw new RuntimeException("Seguro de carga no puede ser inferior a cero"); 
			this.seguroCarga = seguroCarga;
	}
	
/////////////////Getters y Setters//////////////////
	//double gastoComida
	public double GastoComida() {return gastoComida;}
	public void GastoComida(double gastoComida) {this.gastoComida = gastoComida;}
	//double costoFijo
	public double CostoFijo() {return costoFijo;}
	public void CostoFijo(double costoFijo) {this.costoFijo = costoFijo;}
	//boolean largaDistancia
	public boolean LargaDistancia() {return largaDistancia;}
//	public void LargaDistancia(boolean b) {this.largaDistancia = b;} no existe porque largaDistancia es un Final

/////////////////Metodos de clase//////////////////
	@Override
	public void destino(Destino v) {
		if (v.distancia()<=500)
			throw new RuntimeException("El destino no puede tener una distancia menor a 500km");
		this.destino = v;
	}
	@Override
	public double costoViaje() {
		double costo = (this.costoPorKm*this.destino.distancia()) + this.costoFijo + this.gastoComida + this.seguroCarga;
		return costo;
	}

	@Override
	public String tipo() {
		return "MegaTrailer";
	}
	
	
}
