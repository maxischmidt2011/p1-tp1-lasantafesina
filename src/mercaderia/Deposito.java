package mercaderia;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Deposito {
	private int id;
	private boolean frigorifico;
	private boolean propio;
	private double volMax;
	
	private double VolActual;
	private double costo;
	
	private List<Paquete> paquetes;
	
	
	public Deposito(int id, double capacidad, boolean frigorifico, boolean propio, double costo) {
			this.id = id;
			if (volMax < 0)			
				throw new RuntimeException("Litros maximos de capacidad no puede ser inferior a cero"); 
			else
				this.volMax = capacidad;
			this.frigorifico = frigorifico;
			this.propio = propio;
			this.paquetes = new LinkedList<Paquete>();
		if (this.frigorifico && !this.propio)
			this.costo = costo;
		else
			this.costo = 0;
		this.VolActual = 0;
	}
	
/////////////////Getters y Setters//////////////////
	//int id
	public int id() {return this.id;}
	//boolean frigorifico
	public void frigorifico(boolean b) {this.frigorifico = b;}
	public boolean frigorifico() {return this.frigorifico;}
	//boolean propio
	public void propio(boolean b) {this.propio = b;}
	public boolean propio() {return this.propio;}
	//int maxL
	public void volMax(int n) {this.volMax = n;}
	public double volMax() {return this.volMax;}
	//Paquete paquete
	public void paquetes(List<Paquete> p) {this.paquetes = p;}
	public List<Paquete> paquetes() { return this.paquetes;}
	//double costo
	public void costo(double d) {this.costo = d;}
	public double costo() {return this.costo;}
	
/////////////////Metodos de clase//////////////////
	public Boolean agregarPaquete(Paquete p) { 
		double aux = this.VolActual + p.volumen(); 
		if(this.frigorifico() == p.refrigeracion() && aux < this.volMax){
			this.paquetes.add(p);
			this.VolActual = p.volumen();
			return true;
		}
		return false;
	}
	
	public Boolean quitarPaquete(Paquete p) {
		return this.paquetes.remove(p);
	}
	
	public static boolean addPaquete(List<Deposito> depositos, Paquete paquete) {
		Iterator<Deposito> it = depositos.iterator();	
		while(it.hasNext()) {
			Deposito d = it.next();
			if (d.agregarPaquete(paquete)) return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "Deposito [id=" + id + ", frigorifico=" + frigorifico + ", propio=" + propio + ", VolMax=" + volMax
				+ ", VolActual=" + VolActual + ", costo=" + costo + ", paquetes=" + paquetes + "]";
	}
	
}

