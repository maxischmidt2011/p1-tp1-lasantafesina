package mercaderia;

import otros.Destino;

public class Paquete {
	private Destino destino;
	private double volumen;
	private double peso;
	private boolean refrigeracion;
	
	public Paquete(String destino, double volumen, double peso, boolean refrigeracion) {
		this.destino = new Destino();
		this.destino.lugar(destino);
		if (volumen < 0)			
			throw new RuntimeException("Volumen del paquete no puede ser inferior a cero"); 
		this.volumen = volumen;
		if (peso < 0)			
			throw new RuntimeException("Peso del paquete no puede ser inferior a cero"); 
		this.peso = peso;
		this.refrigeracion = refrigeracion;
	}
	
/////////////////Getters y Setters//////////////////
	//string destino
	public Destino destino() {return this.destino;}
	public void destino(String destino) {this.destino.lugar(destino);}
	//double volumen
	public double volumen() {return volumen;}
	public void volumen(double volumen) {this.volumen = volumen;}
	//double peso
	public double peso() {return peso;}
	public void peso(double peso) {this.peso = peso;}
	//boolean refrigeracion
	public boolean refrigeracion() {return refrigeracion;}
	public void refrigeracion(boolean refrigeracion) {this.refrigeracion = refrigeracion;}

	@Override
	public String toString() {
		return "Paquete [destino=" + destino + ", volumen=" + volumen + ", peso=" + peso + ", refrigeracion="
				+ refrigeracion + "]";
	}
	
}
